<?php

namespace Drupal\pepper_views;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\views\Entity\View;

/**
 * Pepper Views Helper.
 */
class PepperViewsHelper {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The view storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorage
   */
  protected $viewStorage;

  /**
   * The view information given by field or parameters.
   *
   * @var array
   */
  protected $viewInformation;

  /**
   * The view.
   *
   * @var \Drupal\views\Entity\View
   */
  protected $view;

  /**
   * The viewExecutable object.
   *
   * @var \Drupal\views\ViewExecutable
   */
  protected $viewExecutable;

  /**
   * The result rows.
   *
   * @var array
   */
  protected $viewResult;

  /**
   * The field config in a json string.
   *
   * @var string
   */
  protected $fieldData;

  /**
   * The view display id string if given.
   *
   * @var string
   */
  protected $viewDisplayId;

  /**
   * The exposed filter information.
   *
   * @var string
   */
  protected $exposedFilter;
  /**
   * The amount of view results.
   *
   * @var int
   */
  protected $viewResultCount;

  /**
   * The view result limit.
   *
   * @var int
   */
  protected $limit;

  /**
   * The view result offset.
   *
   * @var int
   */
  protected $offset;

  /**
   * The current page to show view results.
   *
   * @var int
   */
  protected $page;

  /**
   * The contextual filter values.
   *
   * @var array
   */
  protected $ContextualFilterValues;

  /**
   * The exposed filter values.
   *
   * @var array
   */
  protected $ExposedFilterValues;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */

  protected $renderer;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @codeCoverageIgnore
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RendererInterface $renderer) {
    $this->entityTypeManager = $entityTypeManager;
    $this->viewStorage = $this->entityTypeManager->getStorage('view');
    $this->renderer = $renderer;
  }

  /**
   * Setter for the $view, in case setViewInformation() is not used.
   *
   * @param \Drupal\views\Entity\View $view
   *   The view object.
   */
  public function setView(View $view): void {
    $this->view = $view;
    $this->viewExecutable = $this->view->getExecutable();
  }

  /**
   * Getter for $view.
   *
   * @return \Drupal\views\Entity\View
   *   The view object.
   */
  public function getView(): View {
    return $this->view;
  }

  /**
   * Sets the view, display and some options according to the field settings.
   *
   * @param array $viewInformation
   *   The view information gathered from the view reference field.
   */
  public function setViewInformation(array $viewInformation): void {
    $this->viewInformation = $viewInformation;
    $this->viewDisplayId = $viewInformation['display_id'];
    if(isset($viewInformation['data'])) {
      $this->fieldData = $viewInformation['data'];
    }
    $this->view = $this->viewStorage->load($viewInformation['target_id']);
    $this->viewExecutable = $this->view->getExecutable();
    if(isset($viewInformation['contextual_filter_values']) || isset($this->viewInformation['language'])) {
      if (isset($viewInformation['contextual_filter_values'])) {
        $this->ContextualFilterValues = $viewInformation['contextual_filter_values'];
      }
      else {
        $this->ContextualFilterValues = [];
      }
      $this->attachArguments();
    }
    if(isset($viewInformation['exposed_filter_values'])) {
      $this->ExposedFilterValues = $viewInformation['exposed_filter_values'];
      $this->setExposedFilterInput();
    }
    if ($viewInformation['display_id']) {
      $this->viewExecutable->setDisplay($this->viewDisplayId);
    }
  }

  /**
   * Returns the view id.
   *
   * @return string
   *   The view id.
   */
  public function getViewId(): string {
    return $this->view->id();
  }

  /**
   * Setter for the view result limit.
   *
   * @param int $limit
   *   The view result limit.
   */
  public function setViewLimit(int $limit): void {
    $this->limit = $limit;
    $this->viewExecutable->setItemsPerPage($this->limit);
  }

  /**
   * Setter for the view result offset.
   *
   * @param int $offset
   *   The view result offset.
   */
  public function setViewOffset(int $offset): void {
    $this->offset = $offset;
    $this->viewExecutable->setOffset($this->offset);
  }

  /**
   * Setter for the view results current page.
   *
   * @param int $page
   *   The view result current page.
   */
  public function setViewPage(int $page): void {
    $this->page = $page;
    $this->viewExecutable->setCurrentPage($this->page);
  }

  /**
   * Executes the view.
   */
  private function executeView(): void {
    $this->viewExecutable->execute();
  }

  /**
   * Set exposed filter input.
   */
  private function setExposedFilterInput(): void {
    $this->viewExecutable->setExposedInput($this->ExposedFilterValues);
  }

  /**
   * Attaches contextual filter arguments to the view.
   *
   * @return void
   */
  private function attachArguments(): void {
    $args = [];

    // The language is set for every view automatically.
    if (isset($this->viewInformation['language'])) {
      $args['language'] = $this->viewInformation['language'];
    }

    // Set the rest of the filter values.
    foreach ($this->ContextualFilterValues as $filterName => $value) {
      $args[$filterName] = $value;
    }

    // Set all arguments for contextual filters.
    if (count($args)) {
      $this->viewExecutable->setArguments($args);
    }
  }

  /**
   * Returns the view result count.
   *
   * @return int
   *   The result count.
   */
  public function getViewResultCount(): int {
    $context = new RenderContext();
    $count = $this->renderer->executeInRenderContext($context, function () {
      $this->executeView();
      // @Todo This does not work for the mini pager, see https://www.drupal.org/project/drupal/issues/3265798.
      $this->viewResultCount = $this->viewExecutable->total_rows;
      // This method also don't work for the "Show specific amount"-Pager.
      if ($this->viewExecutable->getPager()->getPluginId() == 'some') {
        $this->viewResultCount = count($this->viewExecutable->result);
      }
      return $this->viewResultCount;
    });

    return $count;

  }

  /**
   * Returns the view results.
   *
   * @return array
   *   The array of result rows.
   */
  public function getViewResult(): array {
    $context = new RenderContext();
    $result = $this->renderer->executeInRenderContext($context, function () {
      // Make sure to get all arguments correct.
      if ($this->limit) {
        $this->viewExecutable->setItemsPerPage($this->limit);
      }
      if ($this->offset) {
        $this->viewExecutable->setOffset($this->offset);
      }
      if ($this->page) {
        $this->viewExecutable->setCurrentPage($this->page);
      }

      $this->executeView();
      $this->viewResult = $this->viewExecutable->result;
      return $this->viewResult;
    });
    return $result;

  }

  /**
   * Return the information about the pager.
   *
   * @return array
   *   The pager information.
   */
  public function getPagerInformation(): array {
    $pager = $this->viewExecutable->getPager();
    $total_pages = 0;
    if (!in_array($pager->getPluginId(), ['none', 'some'])) {
      $total_pages = ceil($this->getViewResultCount() / $pager->getItemsPerPage());
    }
    $pagerInformation = [
      'pager_type' => $pager->getPluginId(),
      'items_per_page' => $pager->getItemsPerPage(),
      'total_pages' => $total_pages,
    ];
    return $pagerInformation;
  }

  public function getContextualFilters() {
    $contextualFilters = [];
    $display = $this->viewExecutable->getDisplay();
    if (isset($display->display['display_options']['arguments']) && !empty($display->display['display_options']['arguments'])) {
      foreach ($display->display['display_options']['arguments'] as $argument) {
        if (isset($argument['default_argument_options']['query_param'])) {
          $contextualFilters[] = $argument['default_argument_options']['query_param'];
        }
        else {
          $contextualFilters[] = $argument['id'];
        }
      }
    }
    return $contextualFilters;
  }

  public function getExposedFilters() {
    $exposedFilters = [];
    $filters = $this->viewExecutable->display_handler->getOption('filters');
    foreach ($filters as $id => $filter) {
      if (isset($filter['exposed']) && $filter['exposed']) {
        $exposedFilter = $this->createExposedFilterOutput($filter);
        $exposedFilters[$id] = $exposedFilter;
      }
    }
    return $exposedFilters;
  }

  /**
   * Creates a valid output for a single filter.
   *
   * @param $filter
   *   The single filter object.
   *
   * @return array
   *   The filter output used by the query.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function createExposedFilterOutput($filter) {
    $exposedFilter = [
      'exposed_filter_type' => $filter['type'],
      'field_name' => $filter['field'],
      'filter_label' => $filter['expose']['label'],
    ];
    // Fill the options for a tag based filter.
    $options = [];
    if ($filter['plugin_id'] == 'taxonomy_index_tid') {
      $termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
      $terms = $termStorage->loadByProperties(['vid' => $filter['vid']]);

      // This only takes place if some terms are not available.
      // But if the $filter['value'] is empty it means, all tags are allowed.
      if (isset($filter['value']) && !(count($filter['value']) == 0)) {
        foreach ($filter['value'] as $value) {
          foreach ($terms as $term) {
            if ($term->id() == $value) {
              if (isset($this->viewInformation['language']) && $term->hasTranslation($this->viewInformation['language'])) {
                $term = $term->getTranslation($this->viewInformation['language']);
              }
              $options[] = [
                'id' => $term->id(),
                'label' => $term->label(),
              ];
            }
          }
        }
      }
      else {
        // All terms are added to the filter.
        foreach ($terms as $term) {
          if (isset($this->viewInformation['language']) && $term->hasTranslation($this->viewInformation['language'])) {
            $term = $term->getTranslation($this->viewInformation['language']);
          }
          $options[] = [
            'id' => $term->id(),
            'label' => $term->label(),
          ];
        }
      }
    }
    if (count($options)) {
      $exposedFilter['options'] = $options;
    }
    return $exposedFilter;
  }

}
