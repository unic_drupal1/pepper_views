<?php

namespace Drupal\pepper_views\Plugin\views\pager;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\pager\SqlBase;

/**
 * Provides a custom LoadMore pager plugin.
 *
 * @ViewsPager(
 *   id = "load_more",
 *   title = @Translation("Load More"),
 *   short_title = @Translation("Load More"),
 *   help = @Translation("Custom Load More pager."),
 * )
 */
class LoadMore extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    if (!empty($this->options['offset'])) {
      return $this->formatPlural($this->options['items_per_page'], 'Load More pager, @count item, skip @skip', 'Load More pager, @count items, skip @skip', ['@count' => $this->options['items_per_page'], '@skip' => $this->options['offset']]);
    }
    return $this->formatPlural($this->options['items_per_page'], 'Load More pager, @count item', 'Load More pager, @count items', ['@count' => $this->options['items_per_page']]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Disable rendering of the "Previous" link.
    $form['tags']['#access'] = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function render($input) {
    return [
      '#theme' => $this->themeFunctions(),
      '#element' => $this->options['id'],
      '#parameters' => $input,
      '#route_name' => !empty($this->view->live_preview) ? '<current>' : '<none>',
    ];
  }

}
