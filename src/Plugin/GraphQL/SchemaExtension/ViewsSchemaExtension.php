<?php

namespace Drupal\pepper_views\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use GraphQL\Error\Error;

/**
 * @SchemaExtension(
 *   id = "pepper_views",
 *   name = "Views schema extension",
 *   description = "Adds views integration.",
 *   schema = "custom_composable"
 * )
 */
class ViewsSchemaExtension extends SdlSchemaExtensionPluginBase {


  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry) {
    $builder = new ResolverBuilder();

    $this->addTypeFields($registry, $builder);
  }

  /**
   * Adds the content fields that are needed to the query.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  protected function addTypeFields(ResolverRegistryInterface $registry, ResolverBuilder $builder) {
    // Resolver for views query.
    $registry->addFieldResolver('Query', 'view',
      $builder->produce('pepper_views_views_query')
        ->map('view_id', $builder->fromArgument('view_id'))
        ->map('view_display_id', $builder->fromArgument('view_display_id'))
        ->map('language', $builder->callback(function ($value, $args, ResolveContext $context) {

          // Allow empty language parameter, as its optional.
          if (empty($args['language'])) {
            return '';
          }

          if (isset($args['language'])) {
            // convert language id to lowercase
            $args['language'] = strtolower($args['language']);
            //replace _ with - in language paramter
            $args['language'] = str_replace('_', '-', $args['language']);
            // check if language exists in drupal.
            if (\Drupal::languageManager()->getLanguage($args['language'])) {
              // set the context language
              $context->setContextLanguage($args['language']);
              return $args['language'];
            }
          }

          throw new Error('Could not resolve language.');

        }))
    );

    $registry->addFieldResolver('ViewsReferenceItem', 'paragraph_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ViewsReferenceItem', '_id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ViewsReferenceItem', 'published',
      $builder->produce('entity_published')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('ViewsReferenceItem', 'title',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_title.value'))
    );

    $registry->addFieldResolver('ViewsReferenceItem', 'view',
      $builder->produce('pepper_views_views_reference')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_view'))
    );

    $registry->addFieldResolver('View', 'view_id',
      $builder->produce('pepper_views_views_id')
        ->map('view_information', $builder->fromParent())
    );

    $registry->addFieldResolver('View', 'view_display',
      $builder->produce('pepper_views_views_display_id')
        ->map('view_information', $builder->fromParent())
    );

    $registry->addFieldResolver('View', 'field_settings',
      $builder->produce('pepper_views_views_reference_field_data')
        ->map('view_information', $builder->fromParent())
    );

    $registry->addFieldResolver('View', 'result_count',
      $builder->produce('pepper_views_views_result_count')
        ->map('view_information', $builder->fromParent())
        ->map('contextual_filter_values', $builder->fromArgument('contextual_filter_values'))
        ->map('exposed_filter_values', $builder->fromArgument('exposed_filter_values'))
    );

    $registry->addFieldResolver('View', 'result',
      $builder->produce('pepper_views_views_result')
        ->map('view_information', $builder->fromParent())
        ->map('limit', $builder->fromArgument('limit'))
        ->map('offset', $builder->fromArgument('offset'))
        ->map('page', $builder->fromArgument('page'))
        ->map('contextual_filter_values', $builder->fromArgument('contextual_filter_values'))
        ->map('exposed_filter_values', $builder->fromArgument('exposed_filter_values'))
    );

    $registry->addFieldResolver('View', 'available_exposed_filters',
      $builder->produce('pepper_views_views_exposed_filter')
        ->map('view_information', $builder->fromParent())
    );

    $registry->addFieldResolver('View', 'available_contextual_filters',
      $builder->produce('pepper_views_views_contextual_filter')
        ->map('view_information', $builder->fromParent())
    );

    $registry->addFieldResolver('View', 'pager',
      $builder->produce('pepper_views_views_pager')
        ->map('view_information', $builder->fromParent())
        ->map('contextual_filter_values', $builder->fromArgument('contextual_filter_values'))
        ->map('exposed_filter_values', $builder->fromArgument('exposed_filter_values'))
    );

  }

}
