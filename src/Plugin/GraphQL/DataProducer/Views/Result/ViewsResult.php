<?php

namespace Drupal\pepper_views\Plugin\GraphQL\DataProducer\Views\Result;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\pepper_views\PepperViewsHelper;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "pepper_views_views_result",
 *   name = @Translation("Views Result"),
 *   description = @Translation("Provides views result."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Result")
 *   ),
 *   consumes = {
 *     "view_information" = @ContextDefinition("any",
 *       label = @Translation("View information"),
 *       required = FALSE
 *     ),
 *    "limit" = @ContextDefinition("integer",
 *       label = @Translation("Limit"),
 *       required = FALSE
 *     ),
 *    "offset" = @ContextDefinition("integer",
 *       label = @Translation("Offset"),
 *       required = FALSE
 *     ),
 *    "page" = @ContextDefinition("integer",
 *       label = @Translation("Page"),
 *       required = FALSE
 *     ),
 *    "contextual_filter_values" = @ContextDefinition("any",
 *       label = @Translation("Contextual filter values"),
 *       required = FALSE
 *     ),
 *    "exposed_filter_values" = @ContextDefinition("any",
 *       label = @Translation("Exposed filter values"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class ViewsResult extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The pepper_view helper service.
   *
   * @var \Drupal\pepper_views\PepperViewsHelper
   */
  protected $pepperViewsHelper;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The view storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorage
   */
  protected $nodeStorage;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\pepper_views\PepperViewsHelper $pepperViewsHelper
   *   The pepper_view helper service.
   *
   * @codeCoverageIgnore
   */
  public function __construct(
    array $configuration,
          $pluginId,
    array $pluginDefinition,
    PepperViewsHelper $pepperViewsHelper,
    EntityTypeManager $entityTypeManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->pepperViewsHelper = $pepperViewsHelper;
    $this->entityTypeManager = $entityTypeManager;
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('pepper_views.pepper_views_helper'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns the results of a view.
   *
   * @param array $viewInformation
   *   View information and field settings.
   * @param ?int $limit
   *   The view result limit.
   * @param ?int $offset
   *   The view result offset.
   * @param ?int $page
   *   The current pager page of the results.
   * @param ?array $contextual_filter_values
   *   The set contextual filter values.
   * @param ?array $exposed_filter_values
   *   The set exposed filter values.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   The field context.
   *
   * @return array
   *   The view result.
   */
  public function resolve(array $viewInformation, ?int $limit, ?int $offset, ?int $page, ?array $contextual_filter_values, ?array $exposed_filter_values, FieldContext $context) {
    $viewInformation['language'] = $context->getContextLanguage();
    if ($contextual_filter_values) {
      $viewInformation['contextual_filter_values'] = $contextual_filter_values;
    }
    if ($exposed_filter_values) {
      $viewInformation['exposed_filter_values'] = $exposed_filter_values;
    }
    $this->pepperViewsHelper->setViewInformation($viewInformation);
    if ($limit) {
      $this->pepperViewsHelper->setViewLimit($limit);
    }
    if ($offset) {
      $this->pepperViewsHelper->setViewOffset($offset);
    }
    if ($page) {
      $this->pepperViewsHelper->setViewPage($page);
    }
    $entities = [];
    foreach ($this->pepperViewsHelper->getViewResult() as $row) {
      if ($viewInformation['target_id'] == 'site_search') {
        $node = $this->nodeStorage->load($row->_entity->id());
      }
      else {
        $node = $this->nodeStorage->load($row->nid);
      }

      try {
        $entities[] = $node->getTranslation($viewInformation['language']);
      } catch (Exception $e) {
        // Leave out untranslated node.
      }
    }
    return $entities;
  }

}
