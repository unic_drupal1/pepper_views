<?php

namespace Drupal\pepper_views\Plugin\GraphQL\DataProducer\Views\Filter;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\pepper_views\PepperViewsHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "pepper_views_views_exposed_filter",
 *   name = @Translation("Views Exposed Filter"),
 *   description = @Translation("Provides views exposed filter."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Exposed filter")
 *   ),
 *   consumes = {
 *     "view_information" = @ContextDefinition("any",
 *       label = @Translation("View information"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class ViewsExposedFilter extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The pepper_view helper service.
   *
   * @var \Drupal\pepper_views\PepperViewsHelper
   */
  protected $pepperViewsHelper;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;


  /**
   * The view storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorage
   */
  protected $nodeStorage;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\pepper_views\PepperViewsHelper $pepperViewsHelper
   *   The pepper_view helper service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   *
   * @codeCoverageIgnore
   */
  public function __construct(
    array $configuration,
          $pluginId,
    array $pluginDefinition,
    PepperViewsHelper $pepperViewsHelper,
    EntityTypeManager $entityTypeManager,
    LanguageManagerInterface $languageManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->pepperViewsHelper = $pepperViewsHelper;
    $this->entityTypeManager = $entityTypeManager;
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('pepper_views.pepper_views_helper'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * Returns the exposed filters of a view.
   *
   * @param array $viewInformation
   *   View information and field settings.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   The field context.
   *
   * @return array
   *   The exposed filters.
   */
  public function resolve(array $viewInformation, $context) {

    // Temporarily alter the language manager, to get translated filer labels from view config.
    $language = $this->languageManager->getLanguage($context->getContextLanguage());
    $original_language = $this->languageManager->getConfigOverrideLanguage();
    $this->languageManager->setConfigOverrideLanguage($language);

    $viewInformation['language'] = $context->getContextLanguage();
    $this->pepperViewsHelper->setViewInformation($viewInformation);
    $filters = $this->pepperViewsHelper->getExposedFilters();

    // Restore the language manager.
    $this->languageManager->setConfigOverrideLanguage($original_language);
    return $filters;
  }

}
