<?php

namespace Drupal\pepper_views\Plugin\GraphQL\DataProducer\Views\Entity;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "pepper_views_views_id",
 *   name = @Translation("Views Id"),
 *   description = @Translation("Provides views id."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "view_information" = @ContextDefinition("any",
 *       label = @Translation("View"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class ViewsId extends DataProducerPluginBase {

  /**
   * Returns an id to a referenced view.
   *
   * @param array $viewInformation
   *   View information and field settings.
   *
   * @return int
   *   The view id.
   */
  public function resolve(array $viewInformation) {
    return $viewInformation['target_id'];
  }

}
