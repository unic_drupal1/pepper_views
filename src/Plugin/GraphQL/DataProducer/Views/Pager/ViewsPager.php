<?php

namespace Drupal\pepper_views\Plugin\GraphQL\DataProducer\Views\Pager;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\pepper_views\PepperViewsHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "pepper_views_views_pager",
 *   name = @Translation("Views Pager"),
 *   description = @Translation("Provides views pager."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Pager")
 *   ),
 *   consumes = {
 *     "view_information" = @ContextDefinition("any",
 *       label = @Translation("View information"),
 *       required = FALSE
 *     ),
 *    "contextual_filter_values" = @ContextDefinition("any",
 *       label = @Translation("Contextual filter values"),
 *       required = FALSE
 *     ),
 *    "exposed_filter_values" = @ContextDefinition("any",
 *       label = @Translation("Exposed filter values"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class ViewsPager extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The pepper_view helper service.
   *
   * @var \Drupal\pepper_views\PepperViewsHelper
   */
  protected $pepperViewsHelper;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The view storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorage
   */
  protected $nodeStorage;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\pepper_views\PepperViewsHelper $pepperViewsHelper
   *   The pepper_view helper service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager service.
   *
   * @codeCoverageIgnore
   */
  public function __construct(
    array $configuration,
          $pluginId,
    array $pluginDefinition,
    PepperViewsHelper $pepperViewsHelper,
    EntityTypeManager $entityTypeManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->pepperViewsHelper = $pepperViewsHelper;
    $this->entityTypeManager = $entityTypeManager;
    $this->nodeStorage = $this->entityTypeManager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('pepper_views.pepper_views_helper'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns the exposed filters of a view.
   *
   * @param array $viewInformation
   *   View information and field settings.
   * @param ?array $contextual_filter_values
   *   The set contextual filter values.
   * @param ?array $exposed_filter_values
   *   The set exposed filter values.
   * @param \Drupal\graphql\GraphQL\Execution\FieldContext $context
   *   The field context.
   *
   * @return array
   *   The exposed filters.
   */
  public function resolve(array $viewInformation, ?array $contextual_filter_values, ?array $exposed_filter_values, FieldContext $context) {
    $viewInformation['language'] = $context->getContextLanguage();
    if ($contextual_filter_values) {
      $viewInformation['contextual_filter_values'] = $contextual_filter_values;
    }
    if ($exposed_filter_values) {
      $viewInformation['exposed_filter_values'] = $exposed_filter_values;
    }
    $this->pepperViewsHelper->setViewInformation($viewInformation);
    return $this->pepperViewsHelper->getPagerInformation();
  }

}
