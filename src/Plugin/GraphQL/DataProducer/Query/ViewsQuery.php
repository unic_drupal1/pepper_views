<?php

namespace Drupal\pepper_views\Plugin\GraphQL\DataProducer\Query;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\FieldContext;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Exception;
use GraphQL\Error\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @DataProducer(
 *   id = "pepper_views_views_query",
 *   name = @Translation("Views query"),
 *   description = @Translation("Provides views query."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "view_id" = @ContextDefinition("any",
 *       label = @Translation("View"),
 *       required = TRUE
 *     ),
 *     "view_display_id" = @ContextDefinition("any",
 *       label = @Translation("View"),
 *       required = TRUE
 *     ),
 *     "language" = @ContextDefinition("any",
 *       label = @Translation("View"),
 *       required = FALSE
 *     ),
 *   }
 * )
 */
class ViewsQuery extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The view storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorage
   */
  protected $viewStorage;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param EntityTypeManager $entityTypeManager
   *   The pepper_view helper service.
   *
   * @codeCoverageIgnore
   */
  public function __construct(
    array $configuration,
          $pluginId,
    array $pluginDefinition,
    EntityTypeManager $entityTypeManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
    $this->viewStorage = $this->entityTypeManager->getStorage('view');
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns information to work with a requested view.
   *
   * @param $view_id
   *   The view id.
   * @param $view_display_id
   *   The view display id.
   * @param FieldContext $context
   *   The field context.
   *
   * @return array
   *   The view information bundled.
   *
   * @throws Error
   */
  public function resolve($view_id, $view_display_id, $language, FieldContext $context) {
    // Try to load the view.
    try {
      $view = $this->viewStorage->load($view_id);
      if ($view instanceof CacheableDependencyInterface) {
        $context->addCacheableDependency($view);
      }
    }
    catch (Exception $e) {
      throw new Error('The view with the id ' . $view_id . ' does not exist.');
    }
    // Try to set the view display.
    try {
      $viewExecutable = $view->getExecutable();
      $viewExecutable->setDisplay($view_display_id);
      $context->addCacheTags($viewExecutable->getCacheTags());
    }
    catch (Exception $e) {
      throw new Error('Can not set the display id ' . $view_display_id . ' on view with id ' . $view_id . '.');
    }

    return [
      'target_id' => $view_id,
      'display_id' => $view_display_id,
      'language' => $context->getContextLanguage(),
    ];
  }

}
