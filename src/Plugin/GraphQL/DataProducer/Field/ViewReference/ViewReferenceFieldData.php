<?php

namespace Drupal\pepper_views\Plugin\GraphQL\DataProducer\Field\ViewReference;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * @DataProducer(
 *   id = "pepper_views_views_reference_field_data",
 *   name = @Translation("Views Reference Field Data"),
 *   description = @Translation("Provides views reference field data."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Element")
 *   ),
 *   consumes = {
 *     "view_information" = @ContextDefinition("any",
 *       label = @Translation("View"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class ViewReferenceFieldData extends DataProducerPluginBase {

  /**
   * Returns the settings and data to a view reference field.
   *
   * @param array $viewInformation
   *   View information and field settings.
   *
   * @return int
   *   The view reference field data.
   */
  public function resolve(array $viewInformation) {
    if (isset($viewInformation['data'])) {
      return $viewInformation['data'];
    }
  }

}
