# pepper_views development version



## Dependencies

* Views (Drupal Core)
* Views Reference Field: https://www.drupal.org/project/viewsreference

## Setup

1. Create a new Paragraph type named Views Reference (views_reference)
2. Add the line ``'views_reference' => 'ViewsReferenceItem'`` to `pepper_graphql/src/Plugin/GraphQL/SchemaExtension/LayoutParagraphsSchemaExtension.php`
3. Give the new Views Reference Paragraph type a new field view (field_view) of type views_reference
4. Create a new view
5. Reference this view into a paragraph

## Example Query with a views reference field

```
{
  router(path: "/en/test-0", language: EN) {
    ... on ErrorResponse {
      code
      message
    }
    ... on RedirectResponse {
      code
      target
    }
    ... on EntityResponse {
      entity {
        ... on ContentPage {
          nid
          content_elements {
            paragraph_id
            ... on ViewsReferenceItem {
              title
              view {
                view_id
                display_id
                field_settings
                result_count(excluded_nid: 82)
                pager(excluded_nid: 82) {
                  items_per_page
                  pager_type
                  total_pages
                }
                result(offset: 2, limit: 5, page: 3, excluded_nid: 82) {
                  ... on ContentPage {
                    title
                    nid
                    langcode
                    author
                    date_published
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
```

## Example views standalone query

```
{
  view(view_id: "test", view_display_id: "block_1", language: DE) {
    view_id
    field_settings
    result_count(excluded_nid: 82)
    pager(excluded_nid: 82) {
      items_per_page
      pager_type
      total_pages
    }
    result(offset: 2, limit: 5, page: 3, excluded_nid: 82) {
      ... on ContentPage {
        title
        nid
        langcode
        author
        date_published
      }
    }
  }
}
```

The `available_exposed_filters` field is not fully working - leave it out if it causes problems.

The pager types are:
 * "some" : show a specific amount of items (can use offset and limit)
 * "none" : all items are shown (can use offset)
 * "full" : an amount of result pages with a set amount of items on each (can use page)
 * "mini" : an amount of result pages with a set amount of items on each (can use page)

To get the results of the view in the context language a contextual filter is needed.
This filter must have the identifier `language` and respond to `translation language`.

To filter out an unwanted node, a contextual filter on `ID (content)` with the exclude flag active must be added in the view.
The parameter must be `excluded_nid`.
