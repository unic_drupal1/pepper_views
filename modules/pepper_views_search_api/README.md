# pepper_views_search_api development version



## Dependencies

* pepper_views
* search_api

## Setup
1. Setup search api in Drupal and index data
2. Configure view to expose search results to Frontend
## Example Query for site_search

```
query ViewQuery($view_id: String!, $view_display_id: String!, $language: LanguageId!, $search_api_fulltext: String) {
  view(view_id: $view_id, view_display_id: $view_display_id, language: $language) {
    result_count(
      contextual_filter_values: {search_api_fulltext: $search_api_fulltext}
    )
    result(contextual_filter_values: {search_api_fulltext: $search_api_fulltext}) {
      nid
      title
      langcode
      ... on ContentPage {
        lead
      }
    }
  }
}

```
Variables:
```
{
  "view_id": "site_search",
  "view_display_id": "search_default",
  "language": "EN",
  "search_api_fulltext": "test"
}
```
