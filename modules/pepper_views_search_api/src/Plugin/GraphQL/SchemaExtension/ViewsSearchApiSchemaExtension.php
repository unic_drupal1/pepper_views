<?php

namespace Drupal\pepper_views_search_api\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use GraphQL\Error\Error;

/**
 * @SchemaExtension(
 *   id = "pepper_views_search_api",
 *   name = "Search API views schema extension",
 *   description = "Adds search api views integration.",
 *   schema = "custom_composable"
 * )
 */
class ViewsSearchApiSchemaExtension extends SdlSchemaExtensionPluginBase {


  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry) {
    $builder = new ResolverBuilder();

    $this->addTypeFields($registry, $builder);
  }

  /**
   * Adds the content fields that are needed to the query.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   The registry of the resolvers.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   The builder of the resolvers.
   */
  protected function addTypeFields(ResolverRegistryInterface $registry, ResolverBuilder $builder) {

  }

}
